package com.atlassian.confluence.plugins.index.api;

/**
 * Field that supports efficient sorting and range queries long values.
 */
public final class LongFieldDescriptor extends FieldDescriptor {
    private final long longValue;

    public LongFieldDescriptor(String name,
                               long value,
                               Store store) {
        super(name, String.valueOf(value), store, Index.ANALYZED);
        longValue = value;
    }

    @Override
    public <T> T accept(FieldVisitor<T> fieldVisitor) {
        return fieldVisitor.visit(this);
    }

    public long longValue() {
        return longValue;
    }
}
