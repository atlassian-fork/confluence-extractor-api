package com.atlassian.confluence.plugins.index.api;

/**
 * Field that supports efficient sorting and range queries double values.
 */
public final class DoubleFieldDescriptor extends FieldDescriptor {
    private final double doubleValue;

    public DoubleFieldDescriptor(String name,
                                 double value,
                                 Store store) {
        super(name, String.valueOf(value), store, Index.ANALYZED);
        doubleValue = value;
    }

    @Override
    public <T> T accept(FieldVisitor<T> fieldVisitor) {
        return fieldVisitor.visit(this);
    }

    public double doubleValue() {
        return doubleValue;
    }
}
