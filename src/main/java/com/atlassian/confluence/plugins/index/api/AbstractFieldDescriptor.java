package com.atlassian.confluence.plugins.index.api;

import com.atlassian.annotations.Internal;

@Internal
abstract class AbstractFieldDescriptor {
    protected final String name;
    protected final String value;
    protected final FieldDescriptor.Index index;
    protected final FieldDescriptor.Store store;

    public AbstractFieldDescriptor(FieldDescriptor.Store store, String name, FieldDescriptor.Index index, String value) {
        this.store = store;
        this.name = name;
        this.index = index;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public FieldDescriptor.Index getIndex() {
        return index;
    }

    public FieldDescriptor.Store getStore() {
        return store;
    }

    public abstract <T> T accept(FieldVisitor<T> fieldVisitor);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FieldDescriptor that = (FieldDescriptor) o;

        if (index != that.index) return false;
        if (!name.equals(that.name)) return false;
        if (store != that.store) return false;
        if (!value.equals(that.value)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + value.hashCode();
        result = 31 * result + index.hashCode();
        result = 31 * result + store.hashCode();
        return result;
    }
}
