package com.atlassian.confluence.plugins.index.api;

/**
 * Field that supports efficient sorting and range queries integer values.
 */
public final class IntFieldDescriptor extends FieldDescriptor {
    private final int intValue;

    public IntFieldDescriptor(String name,
                              int value,
                              Store store) {
        super(name, String.valueOf(value), store, Index.ANALYZED);
        intValue = value;
    }

    @Override
    public <T> T accept(FieldVisitor<T> fieldVisitor) {
        return fieldVisitor.visit(this);
    }

    public int intValue() {
        return intValue;
    }
}
