package com.atlassian.confluence.plugins.index.api;

/**
 * Field that supports efficient sorting and range queries float values.
 */
public final class FloatFieldDescriptor extends FieldDescriptor {
    private final float floatValue;

    public FloatFieldDescriptor(String name,
                                float value,
                                Store store) {
        super(name, String.valueOf(value), store, Index.ANALYZED);
        floatValue = value;
    }

    @Override
    public <T> T accept(FieldVisitor<T> fieldVisitor) {
        return fieldVisitor.visit(this);
    }

    public float floatValue() {
        return floatValue;
    }
}
