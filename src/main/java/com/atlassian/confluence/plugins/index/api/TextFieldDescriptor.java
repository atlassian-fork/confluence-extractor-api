package com.atlassian.confluence.plugins.index.api;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Used for analyzed text.
 */
public final class TextFieldDescriptor extends FieldDescriptor {
    private final AnalyzerDescriptorProvider analyzerProvider;

    /**
     * Create a text field with a custom analyzer.
     *
     * @since 2.0.2
     */
    public TextFieldDescriptor(String name,
                               String value,
                               Store store,
                               AnalyzerDescriptorProvider analyzerProvider) {
        super(name, value, store, Index.ANALYZED);
        this.analyzerProvider = analyzerProvider;
    }

    public TextFieldDescriptor(String name,
                               String value,
                               Store store) {
        this(name, value, store, lang -> Optional.empty());
    }

    @Override
    public <T> T accept(FieldVisitor<T> fieldVisitor) {
        return fieldVisitor.visit(this);
    }

    @Nonnull
    public AnalyzerDescriptorProvider getAnalyzerProvider() {
        return analyzerProvider;
    }
}
